import {combineReducers} from 'redux';

import colors from './colors.reducer'
import selectedColor from './selection.reducer'

const rootReducer = combineReducers({
	colors,
	selectedColor
});

export default rootReducer;



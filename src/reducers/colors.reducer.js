import {FETCH_COLORS_SUCCESS, FETCH_COLORS_FAILURE} from '../actions/index'

export default function (state = {data: [], colorsError: ''}, action) {

	switch (action.type) {
		case FETCH_COLORS_SUCCESS:
			state.data = action.data;
			state.colorsError = action.error;
			return state;
		case FETCH_COLORS_FAILURE: {
			state.data = [];
			state.colorsError = action.error;
			return state;
		}
		default:
			return state;
	}
}




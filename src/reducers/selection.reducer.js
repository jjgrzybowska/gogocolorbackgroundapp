import {SELECT_COLOR} from '../actions/index'

export default function (state = "", action) {
	switch (action.type) {
		case SELECT_COLOR:
			return action.payload;
		default:
			return state;
	}
}


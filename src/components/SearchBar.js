import React from 'react';
import SuggestListContainer from '../containers/SuggestListContainer';
import PropTypes from 'prop-types';
import form from 'bootstrap-css-modules/css/forms.css';
import margin from 'bootstrap-css-modules/css/margin.css';
import btn from 'bootstrap-css-modules/css/buttons.css';

const SearchBar = ({onFormSubmit, onInputChange, fillInput, term, colors}) => {
	const suggestProps = {
		term,
		fillInput,
		colors: colors.data
	};

	const render = (colorsError) => {
		return colorsError ? (<div>Error</div>): (<SuggestListContainer {...suggestProps}/>)
		};

	return (
		<div className={margin.m3}>
			<form onSubmit={onFormSubmit} className={form.inputGroup}>
				<input
					placeholder="Type Color Name"
					className={`${form.formControl}`}
					value={term}
					onChange={onInputChange}
				/>
				<span className={`${form.inputGroupBtn}`}>

						<button type="submit" className={`${btn.btn} ${btn.btnPrimary}`}>Accept</button>
					</span>
			</form>
			{render(colors.colorsError)}
		</div>
	)
};

SearchBar.propTypes = {
	onFormSubmit: PropTypes.func.isRequired,
	onInputChange: PropTypes.func.isRequired,
	fillInput: PropTypes.func.isRequired,
	term: PropTypes.string,
	colors: PropTypes.object.isRequired
};

export default SearchBar;

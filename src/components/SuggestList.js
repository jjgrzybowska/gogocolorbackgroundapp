import React from 'react';
import style from './app.css'
import PropTypes from 'prop-types';
import list from 'bootstrap-css-modules/css/listGroup.css'

const SuggestList = ({handleClick, visible, data}) => {

	const renderSuggest = (item) => {
		return (
			<li className={`${list.listGroupItem} ${list.listGroupItemAction}`} key={item.name}
				onClick={() => handleClick(item)}>
				{item.name}
				<div className={style.colorSwatch} style={{backgroundColor: `#${item.hex}`}}/>
			</li>
		)
	};

	const renderSuggestList = () => {
		return (
			<ul className={`${list.listGroup} ${style.suggestList}`}>
				{data.map(renderSuggest)}
			</ul>
		)
	};

	return(
		<div className={style.suggestContainer}>
			{visible && renderSuggestList()}
		</div>
	)
};

SuggestList.propTypes = {
	handleClick: PropTypes.func.isRequired,
	visible: PropTypes.bool.isRequired,
	data: PropTypes.array,
};


export default SuggestList;



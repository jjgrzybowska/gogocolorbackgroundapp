import React from 'react';
import SearchBarContainer from '../containers/SearchBarContainer';
import PropTypes from 'prop-types';
import style from './app.css'

const App = ({selectedColor}) => {
	return (
		<div className={`${style.containerFull}`} style={{backgroundColor: `#${selectedColor}80`}}>
			<SearchBarContainer/>
		</div>
	);
};

App.propTypes = {
	selectedColor: PropTypes.string,
};

export default App;
import * as actions from '../actions/index';

describe('actions', () => {

	const data = [
		{
			"name": "aliceblue",
			"hex": "f0f8ff"
		},
		{
			"name": "antiquewhite",
			"hex": "faebd7"
		}
	];

	it('should create an action selectColor', () => {

		const expectedAction = {
			type: "SELECT_COLOR",
			payload: "f0f8ff"
		};
		expect(actions.default.selectColor(data, "aliceblue")).toEqual(expectedAction)
	});

	it('should set none to payload', () => {

		const expectedAction = {
			type: "SELECT_COLOR",
			payload: "none"
		};
		expect(actions.default.selectColor(data, "blabla")).toEqual(expectedAction)
	});
});
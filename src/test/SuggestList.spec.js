import React from 'react';
import {shallow} from 'enzyme';
import SuggestList from '../components/SuggestList'
const props = {
	handleClick: jest.fn(),
	visible: true,
	data: [
		{
			"name": "aliceblue",
			"hex": "f0f8ff"
		},
		{
			"name": "antiquewhite",
			"hex": "faebd7"
		},
		{
			"name": "aqua",
			"hex": "00ffff"
		},
		{
			"name": "aquamarine",
			"hex": "7fffd4"
		},
	],
};

it('Renders without crashing', () => {
	const component = shallow(<SuggestList {...props} />);
	expect(component.length).toEqual(1);
});

it('Renders proper amount of suggest/li elements', () => {
	const component = shallow(<SuggestList {...props} />).find('li');
	expect(component.length).toEqual(4);
});

it('Trigger handleClick on item click', () => {
	const component = shallow(<SuggestList {...props} />);
	component.find('li').at(0).simulate('click');
	expect(props.handleClick).toHaveBeenCalled();
});

it('Didnt renders if visible equals false', () => {
	const propsN = {...props, visible: false};
	const component = shallow(<SuggestList {...propsN} />).find('ul');
	expect(component.length).toEqual(0);
});
import React from 'react';
import {shallow} from 'enzyme';
import SearchBar from '../components/SearchBar'
import SuggestListContainer from '../containers/SuggestListContainer'
const props = {
	onFormSubmit: jest.fn(),
	onInputChange: jest.fn(),
	fillInput: jest.fn(),
	term: "",
	colors: {
		data: [
			{
				"name": "aliceblue",
				"hex": "f0f8ff"
			},
			{
				"name": "antiquewhite",
				"hex": "faebd7"
			}
		],
		colorsError: "null"
	},
};


it('Renders without crashing', () => {
	const component = shallow(<SearchBar {...props} />);
	expect(component.length).toEqual(1);
});

it('Trigger handleClick on item click', () => {
	const component = shallow(<SearchBar {...props} />);
	component.find('form').at(0).simulate('submit');
	expect(props.onFormSubmit).toHaveBeenCalled();
});

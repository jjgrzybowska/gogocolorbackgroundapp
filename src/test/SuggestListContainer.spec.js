import React from 'react';
import {shallow} from 'enzyme';
import configureStore from 'redux-mock-store';

import SuggestListContainer from '../containers/SuggestListContainer';
import SuggestList from '../components/SuggestList';

const mockStore = configureStore([]);

const props = {
	colors: [],
	term: "aq",
	fillInput: jest.fn()
};

describe('<SuggestListContainer />', () => {
	it('Should render List.', () => {
		const container = shallow(<SuggestListContainer {...props}/>);
		expect(container.length).toEqual(1);
	});

	it('Should render List.', () => {
		const container = shallow(<SuggestListContainer {...props}/>);
		expect(container.find(SuggestList).length).toEqual(1);
	})
});
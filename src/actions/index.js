import axios from 'axios';

export const FETCH_COLORS_SUCCESS = 'FETCH_COLORS_SUCCESS';
export const FETCH_COLORS_FAILURE = 'FETCH_COLORS_FAILURE';
export const SELECT_COLOR = 'SELECT_COLOR';

const ROOT_URL = `http://www.mocky.io/v2/5a37a7403200000f10eb6a2d`;
const actions = {
		fetchColors: () =>
			dispatch => {
			axios.get(ROOT_URL)
				.then((response) => {
				dispatch({
					type: FETCH_COLORS_SUCCESS,
					data: response.data,
					error: null
				})})
				.catch((error) => {
				dispatch({
					type: FETCH_COLORS_FAILURE,
					data: [],
					error:error
				})});
			},
		selectColor: (data, name) => {
			let res;
			let flteredData = data.filter((obj) => {
				return obj.name === name;
			});

			if (flteredData.length > 0) {
				res = flteredData[0].hex;
			}
			else {
				res = "none"
			}

			return {
				type: SELECT_COLOR,
				payload: res
			}
		}
	};

export default actions;


import React, {Component} from 'react';
import {connect} from 'react-redux';
import App from '../components/App';


class AppContainer extends Component {
	render() {
		return (
			<App selectedColor={this.props.selectedColor}/>
		)
	}
}

function mapStateToProps({selectedColor}) {
	return {selectedColor}
}

export default connect(mapStateToProps)(AppContainer)
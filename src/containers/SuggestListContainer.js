import React, {Component} from 'react';
import SuggestList from '../components/SuggestList';
import PropTypes from 'prop-types';

class SuggestListContainer extends Component {

	constructor() {
		super();
		this.state = {visible: false};
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.term !== this.props.term) {
			this.filter();

			if (this.props.term.length >= 2) {
				if (prevState.visible !== this.state.visible) {
					this.setState({visible: false});
				}
				else {
					this.setState({visible: true});
				}
			} else {
				this.setState({visible: false});
			}
		}
	};

	handleClick = (item) => {
		this.props.fillInput(item);
		this.setState({visible: false});
	};

	filter = () => {
		if (this.props.colors.length) {
			return this.props.colors.filter((item) => {
				let match = item.name.match(this.props.term);
				return (match && match.index === 0);
			});
		}
	};

	render() {
		const props = {
			handleClick: this.handleClick,
			visible: this.state.visible,
			data: this.filter(),
		};

		return (
			<SuggestList {...props}/>
		)
	}
}

SuggestListContainer.propTypes = {
	colors: PropTypes.array.isRequired,
	term: PropTypes.string,
	fillInput: PropTypes.func.isRequired
};


export default SuggestListContainer;




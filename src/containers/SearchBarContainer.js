import React, {Component} from 'react';
import {connect} from 'react-redux';
import actions from '../actions/index';
import PropTypes from 'prop-types';
import SearchBar from '../components/SearchBar';

class SearchBarContainer extends Component {
	constructor(props) {
		super();
		this.state = {term: ''};
		this.onInputChange = this.onInputChange.bind(this);
		this.onFormSumbit = this.onFormSumbit.bind(this);
		this.fillInput = this.fillInput.bind(this);
	}

	componentDidMount() {
		this.props.fetchColors();
	}

	onInputChange(event) {
		this.setState({term: event.target.value});
	};

	onFormSumbit(event) {
		event.preventDefault();
		this.props.selectColor(this.props.colors.data, this.state.term);
		this.setState({term: ''});
	}

	fillInput(data) {
		this.setState({term: data.name});
	}

	render() {
		const props = {
			onFormSubmit: this.onFormSumbit,
			onInputChange: this.onInputChange,
			fillInput: this.fillInput,
			term: this.state.term,
			colors: this.props.colors
		};

		return (
		<SearchBar {...props}/>
		)
	}
}

SearchBarContainer.propTypes = {
	fetchColors: PropTypes.func.isRequired,
	selectColor: PropTypes.func.isRequired,
	colors: PropTypes.object.isRequired,
	selectedColor: PropTypes.string.isRequired,
};


const mapStateToProps = (state) => {
	return {
		colors: state.colors,
		selectedColor: state.selectedColor
	}
};

const mapDispatchToProps = dispatch => ({
	fetchColors: () => dispatch(actions.fetchColors()),
	selectColor: (data, name) => dispatch(actions.selectColor(data, name))
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchBarContainer)
